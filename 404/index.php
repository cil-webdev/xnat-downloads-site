<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - 404 Page Not Found' ;

include($site_root.'/_incl/html_head.php');

?>
</head>
<body id="_404" class="_404">

<?php include($site_root.'/_incl/header_nav.php'); ?>

<div id="page_body"><div class="pad">
    <div class="box">

        <div id="breadcrumbs">
            <ul class="menu horiz">
                <li class="inactive"><a href="/">Home</a></li>
                <li class="active"><a href="#">?</a></li>
            </ul>
            <div class="clear"></div>
        </div>

        <div class="content_left">
            <div class="pad">


                <h1>404 - Not Found</h1>



                <div class="primaryContent">
                    <h2>Sorry, that link didn't go anywhere. </h2>
                    <p>What are you looking for?</p>
                    <p><a href="/documentation">The latest XNAT Documentation</a></p>
                    <p><a href="/news">XNAT News and Events</a></p>
                </div> <!-- end Primary content -->




                <?php
                /*
                <div class="item">
                    <h3><a href="http://website.org/" target="_blank">Words words words words.</a>
                        <i>October 12, 2012</i></h3>
                    <p>Some more words. Some more words. Some more words.</p>
                </div>
                */
                ?>




            </div> <!-- /content_left / pad -->
        </div><!-- /content_left -->


        <div id="sidebar" class="content_right"><div class="pad">
            <div class="box"><div class="box_pad">

                <?php include($site_root.'/_incl/sidebar.php'); ?>

            </div></div>
        </div></div><!-- /content_right -->



        <div class="clear"></div>


    </div><!-- /box -->
    <div class="clear"></div>
</div><!-- /pad --></div><!-- /page_body -->

<div class="clear"></div>

<?php include($site_root.'/_incl/footer.php'); ?>

</body>
</html>
