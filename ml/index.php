<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Get The XNAT Machine Learning Distribution' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="Download XNAT:Machine Learning">
<meta name="keywords" content="XNAT,ML,Machine Learning,AI,model training">
<meta name="google-site-verification" content="RVQOJ9qEDiX-tmJmTleCyBUznCCZ6KQly4-iQ7C4aFs" />
</head>
<body id="download">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>
<?php include($site_root.'/_incl/simple_html_dom.php'); ?>
<?php
/* We use the simple_html_dom plugin to harvest the contents of a large set of auto-generated HTML documentation files
 * One edit was made to the plugin, to preserve /r/n line breaks when importing text
 * Author: S.C. Chen (me578022@gmail.com)
 * Docs and thanks: http://simplehtmldom.sourceforge.net/
 */
?>
<link href="/_css/download-customizations.css" rel="stylesheet" />

<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="https://www.xnat.org">Home</a></li>
                    <li class="inactive"><a href="/">Download</a></li>
                    <li class="active"><a href="#">XNAT ML</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <div class="pad">
                    <h1>Download XNAT Machine Learning</h1>
                </div>
                <div class="pad" style="background-image:url('/images/xnat-ml-bg.png'); background-position: top center; background-repeat:no-repeat; background-size:100%; background-color:#025EA2">
                    <div class="column column-two-thirds" style="height:330px;">
                        &nbsp;
                    </div>
                    <div class="column column-third">
                        <div class="pad">
                            <div style="background: #fff; border: 1px solid #848484; box-shadow: 0 2px 4px rgba(0,0,0,0.4); padding: 15px">
                                <h3>Get XNAT ML</h3>
                                <div class="software-release-packages">
                                    <?php include_once($site_root.'/_incl/download_xnat_ml.php'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>

                </div>
                <div class="pad">
                    <p>The XNAT ML (Beta) release is a stand-alone package that combines a pre-release version of XNAT 1.8 with several platform and plugin additions to enable a baseline of Machine Learning capability.</p>
                    <p>These capabilities were first demonstrated in collaboration with the NVIDIA Clara suite of medical imaging AI tools, at RSNA 2019 (See: <a href="https://wiki.xnat.org/news/blog/2019/12/xnat-and-nvidia-demonstrate-ai-enabled-medical-imaging-at-rsna-2019">XNAT and NVIDIA Demonstrate AI-Enabled Medical Imaging at RSNA 2019</a>). Since then, we have been expanding functionality, deepening integrations, and hardening features to get to the point of this release.</p>
                    <p>Learn more about the bundled components of <strong>XNAT Machine Learning</strong> and its supported workflows here: <strong><a href="https://wiki.xnat.org/ml">XNAT Machine Learning Documentation</a></strong></p>
                </div>
            </div>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<script src="/_js/download_scripts.js"></script>

<?php include($site_root.'/_incl/footer.php'); ?>

<div class="hidden modal-mask"></div>
<div id="license-container" class="hidden modal">
    <p>You must review and agree to the XNAT Software License Agreement before you can download. </p>
    <div style="border: 1px solid #ccc; padding: 10px;" class="frame-container">
        <h2>XNAT Software License Agreement</h2>
        <p>Copyright (c) 2017, Washington University School of Medicine, Harvard University, Howard Hughes Medical Institute. All rights reserved.</p>
        <p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
        <ol>
            <li>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</li>
            <li>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</li>
        </ol>
        <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
    </div>
    <br>
    <p><input id="accept-license" type="button" name="accept" value="I accept the terms and conditions" style="cursor: pointer;" /> <a href="javascript:hideModal()" style="color: #808080; padding-left: 20px;">Cancel</a></p>
</div>
</body>
</html>
