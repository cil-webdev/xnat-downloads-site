$(document).ready(function(){

    // set correct nav elements to 'active'
    var _body_id = $('body').attr('id');
    $('body').addClass(_body_id);
    $('#main_nav .nav.'+_body_id).addClass('active');
    $('#main_nav .subnav.'+_body_id).addClass('active').closest('.nav').addClass('active');
    $('#sidebar .'+_body_id).addClass('active').find('a').addClass('no_link');

    // make sure the subnav is at least as wide as the main nav
    // otherwise it would look pretty stupid
    $('.subnav').each(function(){
        var _nav_width = $(this).closest('.nav').width();
        $(this).css('min-width',_nav_width);
    });

    $('#slider .slide').hide();
    $('#slider .slide').first().show().addClass('first active');
    $('#slider .menu li').first().addClass('first active');
    $('#slider .menu li').last().addClass('last');
    $('#breadcrumbs li').first().addClass('first');
    $('#slider .slide').last().addClass('last');

    var myWidth = function(elem,target,label) {
        var _my_width = $(elem).width();
        $('#news .box_pad '+target).html('The ' + label + ' is ' + _my_width + ' pixels wide.');
    };

    var theWidths = function(){
        myWidth('#main_nav > .pad > ul','.nav_width','main nav');
        myWidth('#slider','.slider_width','slider container');
        myWidth('#news','.news_width','news box');
        myWidth('.icon_boxes:first','.icons_width','icons container');
    };

    // find out the widths when the document loads
    $(window).load(theWidths);

    // if the window is resized, show the slider size
    $(window).resize(theWidths);

    var _box_count = $('#slider .slide').length;
    $('#news .box_pad .slider_boxes').html('There are '+ _box_count + ' slider boxes.')

    var _nav_count = $('#main_nav > .pad > ul > li').length;
    $('#news .box_pad .nav_links').html('There are '+ _nav_count + ' top-level nav links.')

    // click the nav expander on mobile devices
    $('#nav_expand').click(function(){
        if ($(this).hasClass('tapped')){
            $(this).removeClass('tapped');
            $('#main_nav > .pad > ul.menu').removeClass('show');
        }
        else {
            $(this).addClass('tapped');
            $('#main_nav > .pad > ul.menu').addClass('show');
        }
    });

    // simulated breadcrumbs
    // doing these for real now
    /*
    $('#breadcrumbs li').click(function(){
        $(this).removeClass('inactive').addClass('active').nextAll('li').detach();
        $(this).find('a').html('Active');
    });
    */

    // set a default fade speed for stuff that fades
    var fade_speed = 300 ;

    // animate first slider content block on load
    $(window).load(function(){
        $('#slider .slide.first .slide_content').delay(500).fadeIn(500);
    });

    // click slider tabs
    $('#slider .menu li').click(function(){
        if ($(this).hasClass('active')){
            return false ;
        }
        else {
            $('#slider .menu li').removeClass('active');
            $(this).addClass('active');
            var _my_slide = $(this).attr('data-matches');
            $('#slider .slide').fadeOut(fade_speed).removeClass('active');
            $('#slider .slide.'+ _my_slide).fadeIn(fade_speed).addClass('active');
            $('#slider .slide .slide_content').hide();
            $('#slider .slide.' + _my_slide + ' .slide_content').delay(300).fadeIn(500);
        }
    });

    // fade in slider arrows
    $('#slider .slides').hover(
        function(){
            $('#slider .next, #slider .prev').fadeIn(100);
        },
        function(){
            $('#slider .next, #slider .prev').fadeOut(100);
        }
    );

    // click slider arrows
    $('#slider .next, #slider .prev').click(function(){
        var _active_box = $(this).closest('#slider').find('.slide.active');
        var _active_tab = $(this).closest('#slider').find('.menu .active');
        $('#slider .menu li').removeClass('active');
        var fadeActive = function() {
            $(_active_box).fadeOut(fade_speed).removeClass('active');
        };
        if ($(this).hasClass('next')){
            if (_active_box.hasClass('last')){
                $('#slider .slide .slide_content').hide();
                $('#slider .slide.first').fadeIn(fade_speed).addClass('active').find('.slide_content').delay(300).fadeIn(500);
                fadeActive();
                //var _my_tab = $(this).attr('data-matches');
                $('#slider .menu .first').addClass('active');
            }
            else {
                $('#slider .slide .slide_content').hide();
                $(_active_box).next().fadeIn(fade_speed).addClass('active').find('.slide_content').delay(300).fadeIn(500);
                fadeActive();
                $(_active_tab).next().addClass('active');
            }
        }
        if ($(this).hasClass('prev')){
            if (_active_box.hasClass('first')){
                $('#slider .slide .slide_content').hide();
                $('#slider .slide.last').fadeIn(fade_speed).addClass('active').find('.slide_content').delay(300).fadeIn(500);
                fadeActive();
                $('#slider .menu .last').addClass('active');
            }
            else {
                $('#slider .slide .slide_content').hide();
                $(_active_box).prev().fadeIn(fade_speed).addClass('active').find('.slide_content').delay(300).fadeIn(500);
                fadeActive();
                $(_active_tab).prev().addClass('active');
            }
        }

        //if ($('#slider .menu li').attr('data-matches')==_my_tab) {
        //    $('#slider .menu li.'+ _my_tab).addClass('active');
        //}


    });

    // click through slider
    $('#slider .slide h3').each(function(){
        if ($(this).find('a').length){
            $(this).closest('.slide').addClass('linked');
        }
    });
    $('#slider .slide.linked').click(function(){
        var _my_link = $(this).find('h3 a').attr('href');
        if (_my_link != '#') {
            window.location = _my_link ;
        }
        else {
            return false ;
        }
    });


    // click anywhere in sidebar link's box
    $('.content_right dl, .content_right li').click(function(){
        if ($(this).hasClass('no-link')) return false;
        
        var _my_link = $(this).find('a');
        if ((_my_link.attr('href') == '#') || (_my_link.hasClass('no_link')) ){
            return false ;
        }
        else {
            window.location = _my_link.attr('href') ;
        }
    });

    $('.content_right dl, .content_right li').hover(function(){
        if ($(this).hasClass('no-link')) return false;

        if ($(this).find('a').length){
            $(this).css('cursor','pointer');
            $(this).closest('dl,li').addClass('link_hover');
            //$(this).find('a.more i').css('border-bottom-style','solid');
        }
    });

    /*
    $('a.no_link').click(function(){
        return false ;
    });
    */

    $('a').click(function(){
        if (($(this).attr('href')=='#') || ($(this).hasClass('no_link'))){
            return false ;
        }
    })

});


// Cycle stuff
/*
$(document).ready(function() {

    var news_items = $('#news dl').length;

    //$('#slider .box_pad p').delay(500).fadeIn();

    if (news_items > 1) {
        $('#news .box_pad').cycle({
            slideExpr: 'dl' , // if declared, determines which child elements to use
            fx: 'scrollHorz',
            //fx: 'fade' ,
            speed: 300 ,
            timeout: 0 ,
            containerResize: 0 ,
            fit: 0 ,
            //width: '100%' ,
            //easing: 'easeOutQuad' ,
            next: '#news .next a'  //,  click right arrow to advance
            //prev: '#slider .prev' //,   // click left arrow to go back
            //
            //before: function() {
            //    $('#top_slider .wiper1').css('left','1100px').delay(200).animate({ left:'-1100px'}, 1200, 'easeInQuint' );
            //}
        });
        //$('#the_top .left_arrow, #the_top .right_arrow').show();
    }

    //if (news_items <= 1) {
    //$('#the_top .left_arrow, #the_top .right_arrow').hide();
    //}

});
*/
