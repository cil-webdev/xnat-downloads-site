if (typeof XNAT === "undefined") {
    var XNAT = (typeof getObject !== "undefined") ? getObject(XNAT || {}) : {};
}

(function() {

    function queryBitbucketUrl(repo,appended){
        return 'https://api.bitbucket.org/2.0/repositories/'+repo+'/'+appended;
    }

    function downloadLink(file){
        return 'https://www.bitbucket.org/xnatdev/xnat-desktop-client/downloads/'+file;
    }

    XNAT.getDxmVersions = function(){
        var versionNumber = '', releaseDate = '';
        var versions = [
            { os: 'Mac', file: 'latest-mac.yml', linkId: '#download-mac', fileType: '.dmg' },
            { os: 'Windows', file: 'latest.yml', linkId: '#download-win', fileType: '.exe' },
            { os: 'Linux', file: 'latest-linux.yml', linkId: '#download-linux', fileType: '.AppImage' }
        ];
        var headers = {
            'content-type': 'download'
        };

        versions.forEach(function(version){

            try {
                var obj = YAML.load('/desktop-client/'+version.file);
                var $link = $(version.linkId);
                var href = obj.files.filter(function(file){
                    if (file.url.indexOf(version.fileType) >= 0) return file;
                })[0].url;
                $link.prop('href',downloadLink(href))

                versionNumber = obj.version;
                releaseDate = obj.releaseDate;

            } catch(e){
                console.log(e);
            }

        });

        if (versionNumber && releaseDate) {
            releaseDate = new Date(releaseDate).toDateString();
            $('#versionNumber').html('The latest version <strong>'+versionNumber+'</strong> was released on '+releaseDate+'. ');
        }

    }

})();

XNAT.getDxmVersions();