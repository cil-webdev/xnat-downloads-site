var XNAT = (typeof getObject !== "undefined") ? getObject(XNAT || {}) : {};

(function(){
    /*
    Populate download links
     */

    var queryGitHub, queryBitbucket, packages;
    var tagsToIgnore = ['-rc','-demo','-internal'];
    var validFileExtensions = ['.jar','.fatJar','.war'];

    XNAT.packages = packages = []; // import JSON file

    function queryBitbucketUrl(repo,appended){
        return 'https://api.bitbucket.org/2.0/repositories/'+repo+'/'+appended;
    }
    function queryGithubUrl(repo,appended){
        appended = (appended) ? appended : '';
        return 'https://api.github.com/repos/'+repo+'/'+appended;
    }
    function linkLocation(app){
        return $(app.element_id);
    }
    function regExpEscape(s) {
        return !s ? "" : String(s).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, '\\$1').replace(/\x08/g, '\\x08');
    }
    function filterArray(array, property, prefix, suffix) {
        const regex = new RegExp("^" + regExpEscape(prefix) + "\\d[\\.\\d]*\\d" + regExpEscape(suffix) + "$");
        return array.filter(function (item) {
            return item[property].match(regex);
        });
    }
    function sortArray(array, property) {
        return array.sort(function (item1, item2) {
            return item1[property] < item2[property] ? 1 : -1;
        });
    }
    function filterAndSortArray(array, filterProperty, sortProperty, prefix, suffix) {
        return sortArray(filterArray(array, filterProperty, prefix, suffix), sortProperty);
    }

    XNAT.queryBitbucket = queryBitbucket = function(app,downloadFile,tag){
        downloadFile = downloadFile || false;
        tag = tag || false;
        $.ajax({
            url: queryBitbucketUrl(app.repo,'downloads'),
            method: 'GET',
            cache: false
        })
            .success(function(data){
                // get the last element in an ascending array to find the latest release
                console.log(app);
                try {
                    var downloads;
                    if (app.package === 'xnat-web' && tag){
                        downloads = data.values.filter(function(item){
                            return item.name.match(RegExp("^"+app.package+"-"+tag+".war"+"$"));
                        })
                    } else if (app.package === 'xnat-web') {
                        downloads = filterAndSortArray(data.values, "name", "created_on", "xnat-web-", ".war");
                    }
                    else downloads = sortArray(data.values.filter(function(item){
                        var validDownload = false;
                        validFileExtensions.forEach(function(ext){
                            validDownload = validDownload || item.name.endsWith(ext);
                        });
                        tagsToIgnore.forEach(function(tag){
                            validDownload = validDownload && item.name.toLowerCase().indexOf(tag)<0;
                        });
                        return (validDownload) ? item : false;
                        }),"created_on");
                    var latest = downloads[0];
                    var packageUrl = latest.links.self.href;
                }
                catch(e) {
                    console.log(app.package,e);
                    //downloadRepoLink(linkLocation(app),'https://www.bitbucket.org/'+app.repo);
                    var noDownloads = true;
                }

                if (!app.release_id) {
                    // perform another query to get the tag
                    $.ajax({
                        url: queryBitbucketUrl(app.repo,'refs/tags?pagelen=100&sort=-target.date'),
                        method: 'GET',
                        cache: false
                    })
                        .success(function(tagdata){
                            try {
                                var tags = filterAndSortArray(tagdata.values, "name", "name");
                                // remove tags related to non-released versions
                                tags = tags.filter(function(tag){
                                    var tagFound = false;
                                    tagsToIgnore.forEach(function(tagToIgnore){
                                        if (tag.name.toLowerCase().indexOf(tagToIgnore) >= 0) tagFound = true;
                                    });
                                    return (tagFound) ? false : tag;
                                });

                                var tag = tags[0].name;
                                if (noDownloads || app.download_type === 'ziptag') {
                                    packageUrl = "https://www.bitbucket.org/"+app.repo+"/get/"+tag+".zip";
                                }

                                tag = (app['tag_label']) ? app['tag_label'] : tag;
                                downloadLink(downloadFile,linkLocation(app),packageUrl,app.package,tag);
                            }
                            catch(e){
                                console.log(app.package,e);
                                downloadRepoLink(downloadFile,linkLocation(app),'https://www.bitbucket.org/'+app.repo);
                            }
                        })
                        .fail(function(e){
                            console.log(app.package,e);
                            downloadRepoLink(downloadFile,linkLocation(app),'https://www.bitbucket.org/'+app.repo);
                        });
                }
                else {
                    var tag = (app['tag_label']) ? app['tag_label'] : app.release_id;
                    downloadLink(downloadFile,linkLocation(app),packageUrl,app.package,tag);
                }


            })
            .fail(function(e){
                console.log(app.package,e);
                downloadRepoLink(downloadFile,linkLocation(app),'https://www.bitbucket.org/'+app.repo);
            });
    };

    XNAT.queryGitHub = queryGitHub = function(app,downloadFile, tag){
        downloadFile = downloadFile || false;
        tag = tag || false;
        $.ajax({
            method: 'GET',
            url: queryGithubUrl(app.repo,'releases'),
            accepts: { github3: 'application/vnd.github.v3.full+json' },
            cache: false
        })
            .success(function(data){
                // remove tags to ignore
                if (data.length) {
                    data = data.filter(function(item){
                        var tagFound = false;
                        tagsToIgnore.forEach(function(tagToIgnore){
                            if (item['tag_name'].toLowerCase().indexOf(tagToIgnore) >= 0) tagFound = true;
                        });
                        return (tagFound) ? false : item;
                    });
                    data = data.filter(function(item){ if (!item['prerelease']) return item });
                }

                if (data.length){
                    try{
                        // github does not timestamp tags. get the first element and treat it as the latest.
                        var latest = data[0], packageUrl;
                        if (latest.assets.length > 1 && app['asset_name']) {
                            latest.assets.forEach(function(asset){
                                if (asset.name.indexOf(app['asset_name']) > 0) {
                                    packageUrl = asset['browser_download_url'];
                                    return;
                                }
                            });
                        }
                        else if (latest.assets.length === 1) {
                            packageUrl = latest.assets[0]['browser_download_url'];
                        }
                        else {
                            packageUrl = latest['zipball_url'];
                        }
                        var tag = latest['name'] || latest['tag_name'];

                        tag = (app['tag_label']) ? app['tag_label'] : tag;
                        downloadLink(downloadFile,linkLocation(app),packageUrl,app.package,tag);
                    }
                    catch(e){
                        console.log(app.package,e);
                        downloadRepoLink(downloadFile,linkLocation(app),'https://www.github.com/'+app.repo);
                    }
                }
                else {
                    $.ajax({
                        method: 'GET',
                        url: queryGithubUrl(app.repo,'tags'),
                        accepts: { github3: 'application/vnd.github.v3.full+json' },
                        cache: false
                    })
                        .success(function(data){
                            try{
                                // remove release candidate tags
                                data = data.filter(function(item){
                                    var tagFound = false;
                                    tagsToIgnore.forEach(function(tagToIgnore){
                                        if (item['name'].toLowerCase().indexOf(tagToIgnore) >= 0) tagFound = true;
                                    });
                                    return (tagFound) ? false : item;
                                });
                                // data = data.filter(function(item){ if (item['name'].toLowerCase().indexOf('-rc') < 0) return item })

                                // github does not timestamp tags. get the first element and treat it as the latest.
                                var latest = data[0],
                                    tag = latest['name'],
                                    packageUrl = '';

                                if (app['fallback_download']) {
                                    var url = app['fallback_download'];
                                    if (url.split('{tag}').length > 1) {
                                        var urlParts = url.split('{tag}');
                                        url = urlParts[0] + latest['name'] + urlParts[1];
                                    }
                                    packageUrl = url;
                                }
                                else {
                                    packageUrl = latest['zipball_url'];
                                }

                                tag = (app['tag_label']) ? app['tag_label'] : tag;
                                downloadLink(downloadFile,linkLocation(app),packageUrl,app.package,tag);
                            }
                            catch(e){
                                console.log(app.package,e);
                                downloadRepoLink(downloadFile,linkLocation(app),'https://www.github.com/'+app.repo);
                            }
                        })
                        .fail(function(e){
                            console.log(app.package,e);
                            downloadRepoLink(downloadFile,linkLocation(app),'https://www.github.com/'+app.repo);
                        })
                }

            })
            .fail(function(e){
                console.log(app.package,e);
                downloadRepoLink(downloadFile,linkLocation(app),'https://www.github.com/'+app.repo);
            })
    };

    function downloadLink(downloadFile,$linklocation,packageUrl,packageId,tag){
        if (!$linklocation || !packageUrl || !tag) return false;

        if (downloadFile) {
            window.location.assign(packageUrl);
        }
        else {
            $linklocation.empty().append('' +
                '<a class="sla-required download-link" ' +
                'href="'+packageUrl+'">' +
                '<img src="/i/'+packageId+'/'+ tag + '.svg" alt="Download Version '+ tag +'"/>' +
                '</a>');
        }
    }

    function downloadRepoLink(downloadFile,$linklocation,url){
        if (downloadFile) {
            window.location.assign(packageUrl);
        }
        else {
            $linklocation.empty().append('' +
                '<a class="download-tag" ' +
                'href="'+ url + '" ' +
                'target="_blank">' +
                'Download From Repo' +
                '</a>');
        }
    }

    XNAT.downloadPackage = function(packageName,tag) {
        tag = tag || false;
        var downloadFile = true;
        $.getJSON('/_js/download_packages.json')
            .success(function(data){
                XNAT.packages = data;
                XNAT.packages.forEach(function(app){
                    if (app.package === packageName) {
                        // allow custom tag definition
                        if (tag.toLowerCase() !== 'latest') app['release_id'] = tag;

                        if (app.host.toLowerCase() === 'bitbucket') {
                            queryBitbucket(app,downloadFile,tag);
                        }
                        else if (app.host.toLowerCase() === 'github') {
                            queryGitHub(app,downloadFile,tag);
                        }
                    }
                });
            })
            .fail(function(data){
                console.log('Could not load package JSON');
                window.alert('Sorry, an error occurred. Please contact the site administrator.');
            })
    };

    XNAT.initDownloadPage = function(){
        $.getJSON('/_js/download_packages.json')
            .success(function(data){
                XNAT.packages = data;
                XNAT.packages.forEach(function(app){
                    if (app.package === 'xnat-web') {
                        if (!app.tag) {
                            $.ajax({
                                url: queryBitbucketUrl(app.repo,'refs/tags?pagelen=100&sort=-target.date'),
                                method: 'GET',
                                cache: false
                            }).success(function(tagData){
                                var tag = tagData.values[0].name;
                                queryBitbucket(app,false,tag);
                            })
                        }
                        else {
                            queryBitbucket(app,false,app.tag);
                        }
                    }
                    else if (app.host.toLowerCase() === 'bitbucket') {
                        queryBitbucket(app)
                    }
                    else if (app.host.toLowerCase() === 'github') {
                        queryGitHub(app)
                    }
                });
            })
            .fail(function(data){
                console.log('Could not load package JSON')
            })
    };

    /*
    SLA
    */

    // enforce the user's acceptance of the XNAT software license agreement
    $(document).on('click','.sla-required',function(e){
        e.preventDefault();
        var href = $(this).prop('href');
        if (localStorage.getItem('sla-accepted') !== 'true') {
            showSlaModal(href);
            return false;
        } else {
            window.location.assign(href);
        }
    });

    // on license agreement
    $(document).on('click','#accept-license',function(){
        hideModal();
        localStorage.setItem('sla-accepted','true');
        var href = $(this).data('href');
        if (href) window.location.assign(href);
    });

    // if user wishes to click away from the modal
    $(document).on('click','.modal-mask',function(){
        hideModal();
    });

    // display the software license agreement
    var showSlaModal = window.showSlaModal = function(href){
        $('.modal-mask').removeClass('hidden');
        $('.modal').hide().removeClass('hidden').fadeIn(300);
        $('#accept-license').data('href',href);
    };

    var hideModal = window.hideModal = function(){
        $('.modal-mask').addClass('hidden');
        $('.modal').fadeOut(300).addClass('hidden');
    };
})();