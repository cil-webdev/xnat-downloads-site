<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

$page_title = 'XNAT - Download XNAT Utilities' ;

include($site_root.'/_incl/html_head.php');

?>
<meta name="description" content="Download stand-alone utilities to enhance your XNAT functionality, such as the XNAT Upload Assistant">
<meta name="keywords" content="XNAT,anonymization,upload,assistant,applet">
<meta name="google-site-verification" content="RVQOJ9qEDiX-tmJmTleCyBUznCCZ6KQly4-iQ7C4aFs" />
</head>
<body id="download">

<!-- <?php echo ($_SERVER['DOCUMENT_ROOT']); ?> -->

<?php include($site_root.'/_incl/header_nav.php'); ?>
<?php include($site_root.'/_incl/simple_html_dom.php'); ?>
<?php
/* We use the simple_html_dom plugin to harvest the contents of a large set of auto-generated HTML documentation files
 * One edit was made to the plugin, to preserve /r/n line breaks when importing text
 * Author: S.C. Chen (me578022@gmail.com)
 * Docs and thanks: http://simplehtmldom.sourceforge.net/
 */
?>
<link href="/_css/download-customizations.css" rel="stylesheet" />

<div id="page_body">
    <div class="pad">
        <div class="box">

            <div id="breadcrumbs">
                <ul class="menu horiz">
                    <li class="inactive"><a href="https://www.xnat.org">Home</a></li>
                    <li class="active"><a href="#">Download</a></li>
                </ul>
                <div class="clear"></div>
            </div>

            <div class="content_body">
                <div class="pad">
                    <h1>Download XNAT</h1>
                    <div class="column column-two-thirds">
                        <div style="padding: 3em; text-align: center;">
                            <img src="/images/XNAT-logo-980.png" width="490" height="176" alt="XNAT Logo" /><br /><br />
                            <a href="javascript:showSlaModal()">XNAT Software License Agreement</a>
                        </div>
                    </div>
                    <div class="column column-third">
                        <div class="pad">
                            <div style="border: 1px solid #848484; box-shadow: 0 2px 4px rgba(0,0,0,0.4); padding: 20px">
                                <h3>Easy Install With Vagrant</h3>
                                <p><span id="download-xv">...</span> | <a href="https://wiki.xnat.org/documentation/getting-started-with-xnat/running-xnat-in-a-vagrant-virtual-machine" target="_blank" title="XNAT Documentation: Running XNAT with Vagrant">Installation</a></p>
                            </div>
                            <br>
                            <div style="border: 1px solid #848484; box-shadow: 0 2px 4px rgba(0,0,0,0.4); padding: 20px">
                                <h3>Full XNAT Installation</h3>
                                <p>
                                    <strong>XNAT Core:</strong><br>
                                    <span id="download-xnat">...</span> | <a href="https://wiki.xnat.org/documentation/getting-started-with-xnat/xnat-installation-guide" title="XNAT Documentation: XNAT Installation Guide" target="_blank">Installation</a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="content_body">
                <div class="pad">
                    <div class="column column-third">
                        <div class="pad">
                            <h2>Desktop Applications</h2>
                            <hr>
                            <h3>XNAT Desktop Client</h3>
                            <p><strong>Requires XNAT 1.7.5 or later</strong></p>
                            <p>The XNAT Desktop Client is a stand-alone application built with Electron that works with the latest XNAT version to upload image session data with anonymization, and download image session data using a native xnat:// protocol to handle an XML download manifest. The desktop client includes the core functionality of the Upload Assistant, plus transfer management capabilities such as "Pause", "Resume" and "Cancel". </p>
                            <div class="software-release-packages">
                                <?php include_once($site_root.'/_incl/download_desktopclient.php'); ?>
                            </div>
                            <hr>

                            <h3>DICOM Browser</h3>
                            <p>DicomBrowser is an application for inspecting and modifying DICOM metadata in many files at once. A single imaging session can produce thousands of DICOM files; DicomBrowser allows users to view and edit a whole session &ndash; or even multiple sessions &ndash; at once. </p>
                            <p><em>Supports DICOM Edit 4.2 and earlier</em></p>
                            <?php include_once($site_root.'/_incl/download_dicombrowser.php') ?>
                            <p><a href="https://wiki.xnat.org/xnat-tools/dicombrowser">Documentation</a></p>
                            <hr>

<!--                            <h3>XNAT Desktop</h3>-->
<!--                            <p>XNAT Desktop is a graphical user interface purposed for organizing local files of arbitrary content, storing text metadata with managed files, distributed sharing of structured research data and measurements among desktop users, file repository servers and XNAT Enterprise archives.</p>-->
<!--                            <p id="download_xnatd"><a href='https://bintray.com/nrgxnat/generic/xnat-desktop/_latestVersion'><img src='https://api.bintray.com/packages/nrgxnat/generic/xnat-desktop/images/download.svg'></a></p>-->
<!--                            <p><a href="https://wiki.xnat.org/xnat-tools/xnat-desktop">Documentation</a></p>-->
<!--                            <hr>-->

                            <h3>XNAT DICOM Gateway</h3>
                            <p>Use XNAT Gateway to query XNAT for DICOM images from a DICOM viewing workstation. You can install the Gateway locally and connect to it as to any other DICOM server. DICOM requests are translated into XNAT requests, and the result is returned over DICOM networking protocol.</p>
                            <p id="download-gateway"><a href='https://download.nrg.wustl.edu/pub/xnd/download/gw/'><img src='/i/xnat-gateway/latest.svg'></a></p>
                            <p><a href="https://wiki.xnat.org/xnat-tools/xnat-dicom-gateway">Documentation</a></p>

                        </div>
                    </div>

                    <div class="column column-third">
                        <div class="pad">
                            <h2>Core XNAT Plugins</h2>
                            <p>Note that many plugin releases have XNAT version-specific requirements. Read each plugin's documentation carefully before installing. <a href="https://wiki.xnat.org/documentation/xnat-administration/deploying-plugins-in-xnat-1-7">How to Install XNAT Plugins</a></p>
                            <hr>

                            <h3>XNAT OHIF Viewer</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The XNAT OHIF Viewer allows users to view, create and edit ROI collections and annotations on XNAT image sessions. This viewer was developed by the Institute for Cancer Research, London and is based on the Javascript-based OHIF Viewer.</p>
                            <p id="download-ohif-viewer-plugin">...</p>
                            <p><a href="https://bitbucket.org/icrimaginginformatics/ohif-viewer-xnat-plugin" title="XNAT OHIF viewer code on Bitbucket">Source code</a> | <a href="https://wiki.xnat.org/documentation/xnat-ohif-viewer" title="XNAT OHIF viewer documentation on xnat.org">Documentation</a></p>
                            <hr>

                            <h3>Container Service Plugin</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The Container Service plugin allows you to execute external processing tools as lightweight containers, mounting and reading XNAT data, then posting outputs back to your XNAT project. </p>
                            <p id="download-cs">...</p>
                            <p>
                                <a href="https://wiki.xnat.org/container-service/">Getting Started with the Container Service</a>
                            </p>
                            <hr>

                            <h3>Batch Launch Plugin</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The Batch Launch plugin is a companion to the Container Service and allows you to launch containers or pipelines in batches based on project listings or search listings of XNAT data. </p>
                            <p id="download-blp">...</p>
                            <p>
                                <a href="https://wiki.xnat.org/xnat-tools/batch-launch-plugin">Batch Launch Plugin Documentation</a>
                            </p>
                            <hr>

                            <h3>XNAT ML Plugins</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The XNAT Machine Learning suite of plugins enable the core functionality of the XNAT ML workflow. They include a core "ML" plugin and a companion "Datasets" plugin that enables the creation of datasets to perform ML training on. </p>
                            <p id="download-ml">...</p>
                            <p id="download-ml-datasets">...</p>
                            <p>
                                <a href="https://wiki.xnat.org/ml">XNAT ML Documentation</a>
                            </p>
                            <hr>

                            <h3>XNAT Project Sync (XSync)</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>Xsync plugin enables automatic synchronization of data from a project in one XNAT system to a project in a second system. Xsync is configurable to ensure that only the desired data is delivered, and if required, data is properly de-identified, and that it is delivered on a pre-set schedule.</p>
                            <p id="download-xsync">...</p>
                            <p><a href="https://bitbucket.org/xnatdev/xsync">Documentation</a></p>
                            <hr>

                            <h3>LDAP Authentication Plugin</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The LDAP Authentication plugin provides the ability to use one or more LDAP servers to handle your XNAT user authentication and login.</p>
                            <p id="download-ldap-auth-plugin">...</p>
                            <p>
                                <a href="https://bitbucket.org/xnatx/ldap-auth-plugin">Source Code</a> | <a href="https://wiki.xnat.org/documentation/xnat-administration/configuring-authentication-providers">Documentation</a>
                            </p>
                            <hr>

                            <h3>Legacy XNAT Image Viewer Plugin</h3>
                            <p><strong>XNAT version-dependent</strong></p>
                            <p>The legacy XNAT Image Viewer is no longer bundled into XNAT by default, but can be installed as a plugin. It does not support annotations, but it does allow viewing of NIFTI images.</p>
                            <p id="download-legacy-viewer-plugin">...</p>
                            <p>
                                <a href="https://bitbucket.org/xnatdev/xnat-image-viewer-plugin">Source Code / Documentation</a>
                            </p>
                            <hr>

                            <p>Find more XNAT plugins on <a href="https://marketplace.xnat.org" target="_blank">XNAT Marketplace</a></p>
                        </div>
                    </div>

                    <div class="column column-third">
                        <div class="pad">
                            <h2>Developer Resources</h2>
                            <hr>
                            <h3>XNAT Docker-Compose</h3>
                            <p>The XNAT Docker-Compose builder generates a containerized version of XNAT and can be configured to populate your XNAT environment with pre-installed plugins and related dependencies. </p>
                            <p><a href="https://www.github.com/NrgXnat/xnat-docker-compose" id="download-docker-compose" class="download-tag">Download <span class="version-tag">XNAT Docker Compose</span></a></p>
                            <hr>
                            <h3>XNAT Pipeline Engine</h3>
                            <p>The XNAT Pipeline Engine is the legacy means of running processing pipelines on XNAT data. </p>
                            <p id="download-xp">...</p>
                            <p><a href="https://github.com/NrgXnat/xnat-pipeline-engine" title="XNAT Pipeline Engine Source Code on Github" target="_blank">Source Code</a> | <a href="https://wiki.xnat.org/documentation/getting-started-with-xnat/installing-the-pipeline-engine">Documentation</a></p>
                            <hr>

                            <h3>XNAT Populate</h3>
                            <p>XNAT Populate is a project which uploads data to an XNAT instance based on configuration YAML files.</p>
                            <p id="download-xpop"><a href="https://wiki.xnat.org/xnat-tools/xnat-populate">Download</a></p>
                            <p><a href="https://wiki.xnat.org/xnat-tools/xnat-populate">Documentation</a></p>
                            <hr>

                            <h3>DICOM Edit</h3>
                            <p>DicomEdit is a small language for specifying modifications to DICOM metadata. It is used by both DicomBrowser and XNAT for scripted metadata modification. The most up-to-date version of the language is 6.0, which supports editing DICOM sequences, private vendor tags, and many other new features.</p>
                            <p id="download-dcmedit">...</p>
                            <p><a href="https://wiki.xnat.org/xnat-tools/dicomedit">Documentation</a></p>
                            <hr>

                            <h3>XNAT Data Client</h3>
                            <p>The XNAT Data Client supports data transfer operations to and from the XNAT server. This version of the XDC tool has been broken out on its own so that it can be placed anywhere on the user’s system path. The XNAT Data Client replaces the deprecated XNAT REST Client, as of XNAT 1.6.x. </p>
                            <p id="download-xdc">...</p>
                            <p><a href="https://wiki.xnat.org/xnat-tools/xnatdataclient">Documentation</a></p>
                            <hr>

                            <h3>NRG Selenium</h3>
                            <p>NRG_Selenium is a Java framework built on Selenium designed to power automated testing on XNAT-based systems. Current mature projects using the framework include a base XNAT test suite, and CNDA Pipeline tests.</p>
                            <p id="download-sel">...</p>
                            <p><a href="https://wiki.xnat.org/xnat-tools/nrg_selenium">Documentation</a></p>
                            <hr>

                            <h3>XNATpy</h3>
                            <p>XNATpy is an XNAT client developed by ErasmusMC that exposes XNAT objects/functions as Python objects/functions.</p>
                            <p id="download-xpy"><a class="download-tag" href="https://gitlab.com/radiology/infrastructure/xnatpy" target="_blank">Download From Repo</a></p>
                            <p><a href="http://xnat.readthedocs.io/en/latest/">Documentation</a></p>
                            <hr>

                            <h3>XNAT LDAP Vagrant Project</h3>
                            <p>This Vagrant project stands up a simple development LDAP server for use in development and testing.</p>
                            <p id="download-ldap-vagrant-project">...</p>
                            <p><a href="https://bitbucket.org/xnatdev/xnat-ldap-vagrant">Documentation</a></p>
                            <hr>

                            <h3>XNAT Bash Utils</h3>
                            <p>This set of XNAT-aware bash utilities includes XTOLM.</p>
                            <p id="download-xnat-bash-utils">...</p>
                            <p><a href="https://www.github.com/NrgXnat/xnat_bash_utils">Source Code</a> | <a href="https://wiki.xnat.org/xnat-tools/xtolm">Documentation</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clear"></div>

        </div><!-- /box -->
        <div class="clear"></div>
    </div><!-- /pad -->
    <div class="clear"></div>
</div><!-- /page_body -->

<div class="clear"></div>

<script src="/_js/yaml.js"></script> <!-- version 0.30 installed from https://github.com/jeremyfa/yaml.js/tree/develop/dist -->
<script src="/_js/download_scripts.js"></script>
<script src="/_js/dxm_download_script.js"></script>

<script type="application/javascript">
    $(document).ready(function(){
        XNAT.initDownloadPage();
    });
</script>

<?php include($site_root.'/_incl/footer.php'); ?>

<div class="hidden modal-mask"></div>
<div id="license-container" class="hidden modal">
    <p>You must review and agree to the XNAT Software License Agreement before you can download. </p>
    <div style="border: 1px solid #ccc; padding: 10px;" class="frame-container">
        <h2>XNAT Software License Agreement</h2>
        <p>Copyright (c) 2005-<?php echo date("Y"); ?>, Washington University School of Medicine, Harvard University, Howard Hughes Medical Institute. All rights reserved.</p>
        <p>Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:</p>
        <ol>
            <li>Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.</li>
            <li>Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.</li>
        </ol>
        <p>THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</p>
    </div>
    <br>
    <p><input id="accept-license" type="button" name="accept" value="I accept the terms and conditions" style="cursor: pointer;" /> <a href="javascript:hideModal()" style="color: #808080; padding-left: 20px;">Cancel</a></p>
</div>
</body>
</html>
