<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie ie6 no-js"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 no-js"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 no-js"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 no-js"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="UTF-8">

    <title><?php

        if (!isset($page_title)) {
            $page_title = 'XNAT.org' ;
        }

        echo ($page_title) ;

    ?></title>
    <!-- hosted on DV -->

    <!--
    <meta name="viewport" content="width=480;">
    -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1">
    <!--
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    -->
    <script type="text/javascript" src="/_js/jquery.min.js"></script>
    <link rel="stylesheet/less" type="text/css" href="/_css/style.less">
    <script type="text/javascript" src="/_js/less.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/_css/responsive.css">
    <link rel="stylesheet" href="/_css/googlesearch-custom.css" type="text/css">
    <!--
    <script type="text/javascript" src="/_js/jquery.cycle.all.js"></script>
    -->
    <script src="https://use.fontawesome.com/b33b99342a.js"></script>  
    <script type="text/javascript" src="/_js/_scripts.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-1775166-7']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>


<?php
    // closing </head> tag is on each individual template
    // this allows for insertion of page-specific items in the <head>
?>