<?php

if (!isset($site_root)){
    $site_root = $_SERVER['DOCUMENT_ROOT'];
}

?>

<!-- header_nav -->

<div id="wrapper">

<div id="ie8_nag">
    <p>You appear to be using an outdated browser. For the best experience on this site, please install <a href="http://www.google.com/chromeframe/" target="_blank">Chrome Frame</a>, update to <a href="http://www.microsoft.com/ie/" target="_blank">Internet Explorer 8 or later</a>, or use another browser like <a href="http://www.google.com/chrome/" target="_blank">Chrome</a> or <a href="http://www.mozilla.org/" target="_blank">Firefox.</a></p>
</div>

<div id="header"><div class="pad">
    <div class="box">
        <a href="https://www.xnat.org"><img src="/images/XNAT_logo_17.png" width="208" height="82" alt="XNAT"></a>
        <p>open source imaging informatics</p>
    </div>
</div></div>

    <style type="text/css">
        /* #nav_expand { width: 48px ; height: 36px ; line-height: 36px ; vertical-align: middle ; font-size: 20px ; font-weight: bold ; text-align: center ; color: #fff ; border-right: 1px solid #0d3c80 ; } */
    </style>

<div id="main_nav"><div class="pad">
    <div id="nav_expand"><img src="/images/nav_expand2.png" alt=""></div>
    <ul class="menu horiz">
        <li class="nav about"><a class="first" href="https://xnat.org/about/">About XNAT</a>
            <ul class="menu">
                <li class="subnav academy"><a href="/about/xnat-academy.php">XNAT Academy</a></li>
                <li class="subnav who_uses"><a href="https://xnat.org/about/xnat-implementations.php">Who Uses XNAT?</a></li>
                <li class="subnav case_studies"><a href="https://xnat.org/case-studies/">XNAT Case Studies</a></li>
                <li class="subnav tools"><a href="https://xnat.org/about/xnat-tools.php">XNAT Tools</a></li>
                <li class="subnav team"><a href="https://xnat.org/about/xnat-team.php">The XNAT Team</a></li>
                <li class="subnav publications"><a href="https://xnat.org/about/xnat-publications.php">XNAT Publications</a></li>
                <li class="subnav partners"><a href="https://xnat.org/about/partners.php">Partners</a></li>
            </ul>
        </li>
        <li class="nav download"><a href="https://download.xnat.org/">Download</a>
            <ul class="menu">
                <li class="subnav download_xnat"><a href="https://download.xnat.org/">Download XNAT</a></li>
                <li class="subnav download_assistant"><a href="https://download.xnat.org/upload-assistant">XNAT Upload Assistant</a></li>
            </ul>
        </li>
        <li class="nav docs"><a href="https://wiki.xnat.org/documentation">Documentation</a>
            <ul class="menu">
                <li class="subnav xnat_docs"><a href="https://wiki.xnat.org/documentation">XNAT Documentation</a></li>
                <li class="subnav xnat_ml"><a href="https://wiki.xnat.org/ml">XNAT Machine Learning</a></li>
                <li class="subnav xnat_tools"><a href="https://xnat.org/tools">XNAT Tools</a></li>
                <li class="subnav xnat_marketplace_docs"><a href="https://wiki.xnat.org/marketplace-documentation">XNAT Marketplace Documentation</a></li>
            </ul>
        </li>
        <li class="nav news"><a href="https://wiki.xnat.org/news/">News &amp; Events</a>
            <ul class="menu">
                <li class="subnav workshop"><a href="https://www.xnat.org/workshop">XNAT Workshop 2021</a></li>
                <li class="subnav events"><a href="https://www.xnat.org/event-registration">Event Registration</a></li>
            </ul>
        </li>
        <li class="nav marketplace"><a href="http://marketplace.xnat.org/" target="_blank">XNAT Marketplace</a></li>
        <li class="nav contact"><a href="https://xnat.org/contact/">Contact Us</a>
            <ul class="menu">
                <!-- <li class="subnav bugs"><?php /* include($site_root.'/_incl/bug_email.php'); */ ?></li> -->
                <li class="subnav bugs"><a href="javascript:void()" class="report-a-bug">Report A Bug</a></li>
                <li class="subnav developers"><a href="http://groups.google.com/group/xnat_discussion" target="_blank">Developer Community</a></li>
                <li class="subnav mailing_list"><a href="https://xnat.org/contact/mailing-list/">XNAT Mailing List Subscriptions</a></li>
            </ul>
        </li>
    </ul>
    <div class="clear"></div>
</div></div><!-- /main_nav -->
