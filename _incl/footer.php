<?php

    if (!isset($site_root)){
        $site_root = $_SERVER['DOCUMENT_ROOT'];
    }

?>

<style type="text/css">
    .partner_logo { position: relative; top: 7px ; }
</style>

<div id="page_footer">
        <p>XNAT is an open source project produced by NRG at the Washington University School of Medicine | <a href="https://nrg.wustl.edu/" target="_blank">NRG Home</a></p>
        <p class="wikilicense">Contributions to the XNAT Documentation site are licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>. <a rel="license" href="https://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/80x15.png" /></a></p>
</div>

        <div id="pusher"></div>


</div><!-- /wrapper -->

    <div id="sitemap"><div class="pad">

        <ul class="menu">
            <li class="map about"><h4><a href="https://xnat.org/about/">About XNAT</a></h4></li>
            <li class="map who_uses"><a href="https://xnat.org/about/xnat-implementations.php">Who Uses XNAT?</a></li>
            <li class="map case_studies"><a href="https://xnat.org/case-studies/">XNAT Case Studies</a></li>
            <li class="map team"><a href="https://xnat.org/about/xnat-team.php">The XNAT Team</a></li>
            <li class="map publications"><a href="https://xnat.org/about/xnat-publications.php">XNAT Publications</a></li>
            <li class="map partners"><a href="https://xnat.org/about/partners.php">Partners</a></li>
            <li class="map tools"><a href="https://wiki.xnat.org/xnat-tools">XNAT Tools</a></li>
            <li class="map academy"><a href="https://xnat.org/about/xnat-academy.php">XNAT Academy</a></li>

            <li class="map download"><h4><a href="https://download.xnat.org">Download XNAT</a></h4></li>
        </ul>

        <ul class="menu">
            <li class="map docs"><h4><a href="https://xnat.org/documentation">Documentation</a></h4></li>
            <li class="map xnat_ml"><a href="https://wiki.xnat.org/ml">XNAT Machine Learning</a></li>

            <li class="map case_studies"><h4><a href="https://xnat.org/case-studies/">XNAT Case Studies</a></h4></li>
            <li class="map institutional"><a href="https://xnat.org/case-studies/institutional-repositories.php">Institutional Repositories</a></li>
            <li class="map clinical"><a href="https://xnat.org/case-studies/clinical-research.php">Clinical Research</a></li>
            <li class="map multi_center"><a href="https://xnat.org/case-studies/multi-center-studies.php">Multi-Center Studies</a></li>
            <li class="map data_sharing"><a href="https://xnat.org/case-studies/data-sharing.php">Data Sharing</a></li>
        </ul>

        <ul class="menu">
            <li class="map news_events"><h4><a href="https://xnat.org/news/">XNAT News &amp; Events</a></h4></li>
            <li class="map workshop"><a href="https://www.xnat.org/workshop">XNAT Workshop 2021</a></li>
            <li class="map events"><a href="https://www.xnat.org/event-registration">Event Registration</a></li>

            <li class="map marketplace"><h4><a href="http://marketplace.xnat.org/" target="_blank">XNAT Marketplace</a></h4></li>

            <li class="map contact"><h4><a href="https://xnat.org/contact/">Support / Contact Us</a></h4></li>
            <li class="map bugs"><a href="javascript:void()" class="report-a-bug">Report A Bug</a></li>
            <li class="map developers"><a href="http://groups.google.com/group/xnat_discussion" target="_blank">Developer Community</a></li>
            <li class="map mailing_list"><a href="https://xnat.org/contact/mailing-list/">XNAT Mailing List Subscriptions</a></li>
        </ul>

        <ul class="menu">
            <li class="map privacy"><h4><a href="https://xnat.org/privacy/">Privacy Statement</a></h4></li>

        </ul>

        <div class="clear"></div>

    </div></div><!-- /sitemap -->

    <script type="text/javascript">
        // for stuff that needs to happen after everything loads
        $(window).load(function(){

        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1775166-9"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-1775166-9');
    </script>
    <!-- JIRA issue collector -->
    <script type="text/javascript" src="https://issues.xnat.org/s/7dd84e0039c8e4077982b07388626e34-T/en_USu101to/64017/124/1.4.25/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=7bcd3747"></script>
    <script type="text/javascript">window.ATL_JQ_PAGE_PROPS =  {
        "triggerFunction": function(showCollectorDialog) {
            //Requires that jQuery is available!
            jQuery(".report-a-bug").click(function(e) {
                e.preventDefault();
                showCollectorDialog();
            });
        }};
    </script>

