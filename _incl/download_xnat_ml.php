<p><span id="versionNumber"></span></p>
<ul class="software-package-list">

    <li><a href="https://wiki.xnat.org/ml/install" id="download-docker-compose" class="download-tag">Download <span class="version-tag">XNAT ML Docker Compose</span></a></li>
</ul>
<p><a href="https://wiki.xnat.org/ml" target="_blank">Release Notes &amp; Documentation</a> | <a href="https://wiki.xnat.org/ml/components" target="_blank">Components and Sources</a></p>