<?php
$path = $_SERVER['DOCUMENT_ROOT'].'/upload-assistant/index.html';
$html = file_get_html($path);

// list the latest version
foreach ($html->find('p') as $versionText):
    echo '<p>'.$versionText->innertext.'</p>';
    break; // only execute once
endforeach;

// add root directory to package links
foreach ($html -> find('a') as $link) :
    $link->href = '/upload-assistant/'. $link->href; // add root directory to links
endforeach;

foreach ($html -> find('ul li a') as $packageLink) :
    $linktext = $packageLink->innertext;
    if (strpos($linktext,'Debian') > 0) $linktext = 'Linux Debian';

    if ($linktext !== 'SHA256') :
        $packageLink->class = 'download-tag';
        $packageLink->innertext = 'Download<span class="version-tag">'.$linktext.'</span>';
    endif;
endforeach;

// display package links
echo '<ul class="software-package-list">';
foreach ($html -> find('li') as $listItem) :
    echo '<li>';
    echo $listItem-> innertext;
    echo '</li>';
endforeach;
echo '</ul>';
?>