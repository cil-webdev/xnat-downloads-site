<p><span id="versionNumber"></span></p>
<ul class="software-package-list">
    <?php
    /*
     * Use PHP to grab the latest version definitions from YAML files on Bitbucket,
     * Then save them locally so we can parse them in javascript
     */

    $macVersion = 'https://www.bitbucket.org/xnatdev/xnat-desktop-client/downloads/latest-mac.yml';
    $html = file_get_html($macVersion)->save('latest-mac.yml');

    $winVersion = 'https://www.bitbucket.org/xnatdev/xnat-desktop-client/downloads/latest.yml';
    $html = file_get_html($winVersion)->save('latest.yml');

    $lnxVersion = 'https://www.bitbucket.org/xnatdev/xnat-desktop-client/downloads/latest-linux.yml';
    $html = file_get_html($lnxVersion)->save('latest-linux.yml');
    ?>

    <li><a href="#" id="download-mac" class="download-tag">Download <span class="version-tag">Mac OS</span></a></li>
    <li><a href="#" id="download-win" class="download-tag">Download <span class="version-tag">Windows</span></a></li>
    <li><a href="#" id="download-linux" class="download-tag">Download <span class="version-tag">Linux</span></a></li>
</ul>
<p><a href="https://wiki.xnat.org/xnat-tools/DXM" target="_blank">Release Notes &amp; Documentation</a> | <a href="https://bitbucket.org/xnatdev/xnat-desktop-client" target="_blank">Source Code</a></p>