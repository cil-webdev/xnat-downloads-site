<?php
$path = $_SERVER['DOCUMENT_ROOT'].'/dicombrowser/updates.xml';
$entries = simplexml_load_file($path);

echo '<p>The latest version of DICOM Browser is <strong>'.$entries[0]->entry['newVersion'].'</strong>. Download the appropriate installer for your system.</p>';


$versionOrder = ['win32','win64','mac','deb','rpm'];
$versionList = [];

function displayVersion($string){
    $version = false;
    if (strpos($string,'x86') > 0)  $version = array ("key" => "win32", "value" => "Windows (32-bit)");
    if (strpos($string,'x64') > 0)  $version = array ("key" => "win64", "value" => "Windows (64-bit)");
    if (strpos($string,'macos') > 0) $version= array ("key" => "mac", "value" => "OS X");
    if (strpos($string,'.rpm') > 0) $version = array ("key" => "rpm", "value" => "Linux RPM");
    if (strpos($string,'.deb') > 0) $version = array ("key" => "deb", "value" => "Linux Debian");
    return $version;
}

// parse each version listing from the XML document
foreach ($entries as $entry):
    $url = '/dicombrowser/'.$entry['fileName'];
    $versionLabel = displayVersion($entry['fileName']);
    if ($versionLabel) array_push($versionList,
        '<a class="download-tag" href="'.$url.'">Download <span class="version-tag">'.$versionLabel['value'].'</span></a>'
    );
endforeach;

// display package links
echo '<ul class="software-package-list">';
foreach ($versionList as $v) :
    echo '<li>'.$v.'</li>';
endforeach;
echo '</ul>';
?>